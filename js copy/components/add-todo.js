import Alert from './alert.js';

export default class AddTodo {
  constructor() {
    this.btn = document.getElementById('add');
    this.title = document.getElementById('title');
    this.email = document.getElementById('email');
    this.phone = document.getElementById('phone');
    this.date = document.getElementById('date');
    this.time = document.getElementById('time');
    this.description = document.getElementById('description');

    this.alert = new Alert('alert');
  }

  onClick(callback) {
    this.btn.onclick = () => {
      if (title.value === '' || email.value === '') {
        this.alert.show('Llene los campos vacios');
      } else {
        this.alert.hide();
        callback(this.title.value, this.email.value, this.phone.value, this.date.value, this.time.value, this.description.value);
      }
    }
  }
}
