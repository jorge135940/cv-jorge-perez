import Alert from './alert.js';

export default class Modal {
  constructor() {
    this.title = document.getElementById('modal-title');
    this.email = document.getElementById('modal-email');
    this.phone = document.getElementById('modal-phone');
    this.date = document.getElementById('modal-date');
    this.time = document.getElementById('modal-time');
    this.description = document.getElementById('modal-description');
    this.btn = document.getElementById('modal-btn');
    this.completed = document.getElementById('modal-completed');
    this.alert = new Alert('modal-alert');

    this.todo = null;
  }

  setValues(todo) {
    this.todo = todo;
    this.title.value = todo.title;
    this.email.value = todo.email;
    this.phone.value = todo.phone;
    this.date.value = todo.date;
    this.time.value = todo.time;
    this.description.value = todo.description;
    this.completed.checked = todo.completed;
  }

  onClick(callback) {
    this.btn.onclick = () => {
      if (!this.title.value || !this.email.value || !this.phone.value || !this.date.value || !this.time.value || !this.description.value) {
        this.alert.show('Llene los campos vacíos');
        return;
      }

      $('#modal').modal('toggle');

      callback(this.todo.id, {
        title: this.title.value,
        email: this.email.value,
        phone: this.phone.value,
        date: this.date.value,
        time: this.time.value,
        description: this.description.value,
        completed: this.completed.checked,
      });
    }
  }
}
